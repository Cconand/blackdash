import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { Produit } from 'src/app/model/produit';
import { ProductService } from 'src/app/service/product.service';

@Component({
  selector: 'app-produit',
  templateUrl: './product.component.html',
  
})
export class ProduitComponent implements OnInit {

  produits: any[];
  produit: Produit = new Produit();
  constructor(private produitService : ProductService, private router : Router) { }

  ngOnInit(): void {
    this.chargerProduits();
  }
  /*onDispoProduit(){
    this.produitService.disoProduit();
  }
  onIndispoProduit(){
    this.produitService.indisoProduit();
  }*/
  chargerProduits(){
    this.produitService.getAllProduit().subscribe(data => {this.produits = data});
  }
  saveProduit(){
    this.produitService.saveProduit(this.produit).subscribe( () => {
      this.chargerProduits(); //Charger la liste
      this.produit = new Produit(); //Vider le formulaire
    });
  }
  deleteProduit(produit){
    this.produitService.deleteProduit(produit.idProduit).subscribe(() => {
      this.chargerProduits();

    });
  }
  editProduit(id:number){ 
    this.router.navigate(['editProduit',id]);
  }
}
