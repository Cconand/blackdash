import { Component, OnInit } from "@angular/core";
import { Utilisateur } from 'src/app/model/utilisateur';
import { UserService } from 'src/app/service/user.service';
import { Router } from '@angular/router';

@Component({
  selector: "app-user",
  templateUrl: "user.component.html"
})
export class UserComponent implements OnInit {
  users: any[];
  user: Utilisateur = new Utilisateur();
  constructor(private userService : UserService, private router : Router) { }
  

  ngOnInit(): void {
    //this.users = this.userService.users;
    this.chargerUtilisateurs();
  }
 /* onActiverUser(){
    this.userService.activerUser();
  }
  onDesactiverUser(){
    this.userService.desactiverUser();
  }*/
  chargerUtilisateurs(){
    this.userService.getAllUtilisateur().subscribe(data => {this.users = data});
  }
  saveUtilisateur(){
    this.userService.saveUtilisateur(this.user).subscribe( () => {
      this.chargerUtilisateurs(); // Charger la liste
      this.user = new Utilisateur(); // Vider le formulaire
    });
  }
  deleteUtilisateur(user){
    this.userService.deleteUtilisateur(user.idUtilisateur).subscribe(()=>{
      this.chargerUtilisateurs();
    });
  }
  editUser(id:number){ 
    this.router.navigate(['editUser',id]);
  }
}
